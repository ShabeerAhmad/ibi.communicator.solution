package shared;

import java.util.Date;

public class POSData {

	/*** Private variables ***/
	private Date updateTime;
	private String clock;
	private double latitude;
	private double longitude;
	private String line;
	private String destination;
	private String nextStop;
	private String zone;
	private boolean signOverwritten;
	private String schedule;

	private String rawData;

	/*** Getters / Setters ***/
	public Date getUpdateTime()
	{
		return this.updateTime;
	}

	public String getSchedule()
	{
		return this.schedule;
	}

	public void setSchedule(String value)
	{
		this.schedule = value;
	}
	
	public String getClock()
	{
		return this.clock;
	}

	public void setClock(String value)
	{
		this.clock = value;
	}
	
	public double getLatitude()
	{
		return this.latitude;
	}

	public void setLatitude(double value)
	{
		this.latitude = value;
	}

	public double getLongitude()
	{
		return this.longitude;
	}

	public void setLongitude(double value)
	{
		this.longitude = value;
	}

	public String getLine()
	{
		return this.line; //
	}

	public void setLine(String value)
	{
		this.line = value;
	}

	public String getDestination()
	{
		return this.destination;
	}

	public void setDestination(String value)
	{
		this.destination = value;
	}

	public String getNextStop()
	{
		return this.nextStop;
	}

	public void setNextStop(String value)
	{
		this.nextStop = value;
	}

	public String getZone()
	{
		return this.zone;
	}

	public void setZone(String value)
	{
		this.zone = value;
	}

	public boolean getSignOverwritten()
	{
		return this.signOverwritten;
	}

	public void setSignOverwritten(boolean value)
	{
		this.signOverwritten = value;
	}

	public String getRawData()
	{
		return this.rawData;
	}

	public void setRawData(String value)
	{
		this.rawData = value;
	}

	public POSData()
	{
		this.updateTime = new Date();
	}
	
	public boolean isTestOK()
	{
		if(!ApplicationUtility.isNullOrEmpty(this.rawData))
			return this.rawData.toLowerCase().substring(this.rawData.indexOf("test=") + 5).trim().equals("ok");

		return false;
	}
}
