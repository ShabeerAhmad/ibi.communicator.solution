package pushtotalk;

import java.util.Timer;
import java.util.TimerTask;

import javax.websocket.Session;

import models.CommandsHandler;
import models.WebsocketSession;
import shared.ApplicationUtility;
import shared.Logger;
import websocket.WebSocketServerEndpoint;

public class PushtoTalkHandler 
{
	private static Timer PushtoTalkTimer;
	
	public PushtoTalkHandler() {}
	
	public static void initialize()
	{
		Logger.write("communication_clientservice", "--------------PushtoTalk process initialized---------------------------------");
		PushtoTalkTimer = new Timer();
		PushtoTalkTimer.schedule(new TimerTask(){
			@Override
			public void run()
			{
				try
				{

					WebsocketSession ws = WebSocketServerEndpoint.Sessions;
					Session s = ws.getSession();
					String lastMessage = ws.getMessage();
					try
					{
					    if (s != null && s.isOpen()) 
				        {
							String panicContent = ApplicationUtility.readPushtoTalkFileContents();
							//if (!lastPanicState.equalsIgnoreCase(panicContent))
							if (!ApplicationUtility.isNullOrEmpty(panicContent) && !lastMessage.equalsIgnoreCase(panicContent))
							{
								String pushtoTalkCommand = CommandsHandler.getPushtoTalkCommandForUI(panicContent);
								s.getBasicRemote().sendText(pushtoTalkCommand);
								ws.setMessage(panicContent);
								Logger.write("communication_clientservice_websocket", "Websocket to UI >> data send : " + pushtoTalkCommand);
							}
						}
						else
						{
							if (s != null)
								s.close();
						}
					}
					catch(Exception ex)
					{
						Logger.writeError("communication_clientservice_websocket", ex);
						if (s != null)
							s.close();
					}

				}
				catch(Exception ex)
				{
					Logger.writeError("communication_clientservice", ex);
				}	
			}
		}, 1 * 500, 1 * 500);
	}
}
