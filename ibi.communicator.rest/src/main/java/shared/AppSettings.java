package shared;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Properties;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;

import shared.Logger;

public class AppSettings 
{
	private static String getConfigsDirectory()
	{
		try 
		{
			File applicationDirectory = new File(System.getProperty("user.dir"));
			String dataPath = applicationDirectory + "/config";
			applicationDirectory = new File(dataPath);
			if (!applicationDirectory.exists())
				applicationDirectory.mkdir();
			return dataPath;
		}
		catch (Exception e) 
		{
			Logger.writeError("communication_rest", e);
		}
		return "";
	}

	public static String getRestConfigFilePath()
	{
		return getConfigsDirectory() + "/config.xml";
	}
	
	public static void ensureDirectoryExist(String path)
	{
		ArrayList<String> missingPaths = new ArrayList<String>();

		File currentDirectory = new File(path);

		while(!currentDirectory.exists())
		{
			missingPaths.add(0, currentDirectory.getAbsolutePath());

			currentDirectory = new File(currentDirectory.getParent());
		}

		for(int i = 0; i < missingPaths.size(); i++)
		{			
			currentDirectory = new File(missingPaths.get(i));

			currentDirectory.mkdir();
		}
	}
	private static Document getClientConfigurationDocument()
	{	
		Document xmlDocument = null;

		try
		{
			File xmlFile = new File(getRestConfigFilePath());

			ensureDirectoryExist(xmlFile.getParent());

			if(!xmlFile.exists())
				return null;
			
			FileInputStream settingsStream = new FileInputStream(xmlFile.getAbsolutePath());

			DataInputStream in = new DataInputStream(settingsStream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String line;
			String settingsData = "";

			while((line = br.readLine()) != null)
			{
				settingsData += line;	
			}
				
			in.close();
			settingsStream.close();
			
			SAXBuilder builder = new SAXBuilder();
			//Document xmlDocument = builder.build(xmlFile.getAbsolutePath());
			xmlDocument = builder.build(new StringReader(settingsData));
			
		}
		catch(Exception ex)
		{
			Logger.writeError("ibi", ex);
			xmlDocument = null;
		}
		
		return xmlDocument;
	}
	private static String getConfigValue(String path)
	{
		try
		{
			Document xmlDocument = getClientConfigurationDocument();
			Element node = null;
			
			if(xmlDocument != null)
			{
				node = (Element) XPath.newInstance(path).selectSingleNode(xmlDocument);
				node = node.getParentElement().getChild("Value");
				//node = ensureNodeExists(xmlDocument, path, false);
			}
			
			if(node != null)
			{
				return node.getText().trim();
			}
			else
				return "";	
		}
		catch(Exception ex)
		{
			return "";
		}	
	}
	public static String getDatabaseName()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'Database_Name']");
		return (value == null || value.equals("")) ? "jdbc:mysql://localhost/communication" : value;
	}
	public static String getDatabaseUser()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'Database_User']");
		return (value == null || value.equals("")) ? "communication" : value;
	}
	public static String getDatabaseUserPassword()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'Database_User_Password']");
		return (value == null || value.equals("")) ? "ibi0786" : value;
	}
}
