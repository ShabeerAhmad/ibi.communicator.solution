package shared;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;

import shared.Logger;

public class ClientConfigurations 
{
	/*** Private methods ***/
	public static String resolveRootPath()
	{
		String osName = System.getProperty("os.name");

		if(osName.toLowerCase().indexOf("windows") != -1)
			return "C:";
		else
			return "";
	}

	public static String getIBIConfigFilePath()
	{
		//return resolveRootPath() + "/storage/config/ibiconfig.xml";
		return resolveRootPath() + "/storage/config/ibiconfig.xml";
	}
	public static void ensureDirectoryExist(String path)
	{
		ArrayList<String> missingPaths = new ArrayList<String>();

		File currentDirectory = new File(path);

		while(!currentDirectory.exists())
		{
			missingPaths.add(0, currentDirectory.getAbsolutePath());

			currentDirectory = new File(currentDirectory.getParent());
		}

		for(int i = 0; i < missingPaths.size(); i++)
		{			
			currentDirectory = new File(missingPaths.get(i));

			currentDirectory.mkdir();
		}
	}
	private static Document getClientConfigurationDocument()
	{	
		Document xmlDocument = null;

		try
		{
			File xmlFile = new File(getIBIConfigFilePath());

			ensureDirectoryExist(xmlFile.getParent());

			if(!xmlFile.exists())
				return null;
			
			FileInputStream settingsStream = new FileInputStream(xmlFile.getAbsolutePath());

			DataInputStream in = new DataInputStream(settingsStream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String line;
			String settingsData = "";

			while((line = br.readLine()) != null)
			{
				settingsData += line;	
			}
				
			in.close();
			settingsStream.close();
			
			SAXBuilder builder = new SAXBuilder();
			//Document xmlDocument = builder.build(xmlFile.getAbsolutePath());
			xmlDocument = builder.build(new StringReader(settingsData));
			
		}
		catch(Exception ex)
		{
			Logger.writeError("communication_rest", ex);
			xmlDocument = null;
		}
		
		return xmlDocument;
	}
	private static String getConfigValue(String path)
	{
		try
		{
			Document xmlDocument = getClientConfigurationDocument();
			Element node = null;
			
			if(xmlDocument != null)
			{
				node = (Element) XPath.newInstance(path).selectSingleNode(xmlDocument);
				node = node.getParentElement().getChild("Value");
				//node = ensureNodeExists(xmlDocument, path, false);
			}
			
			if(node != null)
			{
				return node.getText().trim();
			}
			else
				return "";	
		}
		catch(Exception ex)
		{
			//Logger.writeError("ibi", ex);
			Logger.writeError("communication_rest", ex);
			return "";
		}	
	}
	
	public static String getLanguage()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_Culture']");

		return (value == null || value.equals("")) ? "en-US" : value;
	}
	
	public static int getAgeToOffline()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'CCU_AgeToOffline_Minutes']");
		return (value == null || value.equals("")) ? 1 : Integer.parseInt(value);	
	}
	
	public static Boolean getClockShowSeconds()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_ShowSeconds']");
		return (value == null || value.equals("")) ? false : Boolean.parseBoolean(value);	
	}	
	
	public static String getMoviaJourneySelectionModuleOnAcceptAction()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_MoviaJourneySelectionModule_OnAcceptAction']");
		return (value == null || value.equals("")) ? "main" : value;	
	}
	
	public static String getChangeSignModuleOnAcceptAction()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_ChangeSignModule_OnAcceptAction']");
		return (value == null || value.equals("")) ? "main" : value;	
	}
	
	public static int getDestinationListFetchInterval()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_DestinationListFetchInterval_Minutes']");
		return (value == null || value.equals("")) ? 30 : Integer.parseInt(value);	
	}
	
	public static int getDistanceModuleMaxDistnaceToDisplay()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_DistanceModule_MaxDistnaceToDisplay_Minutes']");
		return (value == null || value.equals("")) ? 20 : Integer.parseInt(value);	
	}	
	
	public static int getDistanceModuleMaxDistnaceAgeToDisplay()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_DistanceModule_MaxDistnaceAgeToDisplay_Minutes']");
		return (value == null || value.equals("")) ? 5 : Integer.parseInt(value);	
	}
	
	public static int getIncomingcallsAutomaticDecline()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_Incomingcalls_AutomaticDecline_Seconds']");
		return (value == null || value.equals("")) ? 30 : Integer.parseInt(value);	
	}
	
	public static Boolean getJourneySelectorModuleEnabled()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_JourneySelectionModule_Enable']");
		return (value == null || value.equals("")) ? false : Boolean.parseBoolean(value);	
	}
	public static int getModuleOrderJourneySelector()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_ModuleOrder_JourneySelection']");

		return (value == null || value.equals("")) ? 60 : Integer.parseInt(value);	
	}
	public static Boolean getSignsModuleEnabled()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_SignsModule_Enable']");

		return (value == null || value.equals("")) ? true : Boolean.parseBoolean(value);	
	}
	public static int getModuleOrderSigns()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_ModuleOrder_Signs']");

		return (value == null || value.equals("")) ? 90 : Integer.parseInt(value);	
	}
	
	public static Boolean getSignAddonsModuleEnabled()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_SignAddonsModule_Enable']");
		/*if (AppSettings.isDebugMode())	value = "true";*/
		return (value == null || value.equals("")) ? false : Boolean.parseBoolean(value);	
	}
	public static int getModuleOrderSignAddons()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_ModuleOrder_SignAddons']");
		return (value == null || value.equals("")) ? 80 : Integer.parseInt(value);	
	}
	public static Boolean getLandToBusModuleEnabled()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_LandToBusModule_Enable']");

		return (value == null || value.equals("")) ? false : Boolean.parseBoolean(value);	
	}
	public static int getModuleOrderLandToBus()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_ModuleOrder_LandToBus']");

		return (value == null || value.equals("")) ? 70 : Integer.parseInt(value);	
	}
	public static Boolean getBusToLandModuleEnabled()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_BusToLandModule_Enable']");

		return (value == null || value.equals("")) ? false : Boolean.parseBoolean(value);	
	}
	public static int getModuleOrderBusToLand()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_ModuleOrder_BusToLand']");

		return (value == null || value.equals("")) ? 10 : Integer.parseInt(value);	
	}
	public static Boolean getJourneyProgressEnabled()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_JourneyProgressModule_Enable']");

		return (value == null || value.equals("")) ? false : Boolean.parseBoolean(value);	
	}
	public static int getModuleOrderJourneyProgressModule()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_ModuleOrder_JourneyProgressModule']");

		return (value == null || value.equals("")) ? 50 : Integer.parseInt(value);	
	}
	public static Boolean getVehicleScheduleModuleEnabled()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_VehicleScheduleModule_Enable']");

		return (value == null || value.equals("")) ? false : Boolean.parseBoolean(value);	
	}
	
	public static int getModuleOrderVehicleScheduleModule()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_ModuleOrder_VehicleScheduleModule']");

		return (value == null || value.equals("")) ? 30 : Integer.parseInt(value);	
	}
	public static Boolean getMoviaJourneyModuleEnabled()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_MoviaTripSelectionModule_Enable']");

		return (value == null || value.equals("")) ? false : Boolean.parseBoolean(value);	
	}
	public static int getModuleOrderMoviaTripSelectionModule()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_ModuleOrder_MoviaTripSelectionModule']");

		return (value == null || value.equals("")) ? 40 : Integer.parseInt(value);	
	}
	
	public static Boolean getVAModuleEnabled()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_VAModule_Enable']");

		return (value == null || value.equals("")) ? false : Boolean.parseBoolean(value);	
	}
	
	public static int getModuleOrderVA()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_ModuleOrder_VA']");

		return (value == null || value.equals("")) ? 100 : Integer.parseInt(value);	
	}
	public static Boolean getHotSpotModuleEnabled()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_HotspotModule_Enable']");

		return (value == null || value.equals("")) ? false : Boolean.parseBoolean(value);	
	}
	public static int getModuleOrderHotspot()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_ModuleOrder_Hotspot']");

		return (value == null || value.equals("")) ? 20 : Integer.parseInt(value);	
	}
	
	public static int getMoviaCallsTimeout()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_MOVIAJOURNEY_CallsTimeoutSeconds']");

		return (value == null || value.equals("")) ? 10 : Integer.parseInt(value);	
	}
	
	public static String getBackendServiceResourecePath()
	{
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_BackendServiceResources_Path']");

		return (value == null || value.equals("")) ? "/storage/system/dcu/Data" : value;
	}
	
	public static int[] getDCULightDayNight()
	{
		int dcudaynight[] = new int[2];
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'DCU_Light_DayNight']");

		if (value == null || value.equals(""))
		{
			//default
			dcudaynight[0] = 100;
			dcudaynight[1] = 100;
		}
		else
		{
			value = value.replace("[", "");
			value = value.replace("]", "");
			String[] values = value.split(";");
			if (values.length != 2)
			{
				//default
				dcudaynight[0] = 100;
				dcudaynight[1] = 100;
			}
			else
			{
				try
				{
					dcudaynight[0] = Integer.parseInt(values[0]);
					dcudaynight[1] = Integer.parseInt(values[1]);
				}
				catch(Exception ex)
				{
					//default
					dcudaynight[0] = 100;
					dcudaynight[1] = 100;
				}
			}

		}
		return dcudaynight;
	}
	
	
	public static int[] getVTCLightDayNight()
	{
		int vtcdaynight[] = new int[2];
		String value = getConfigValue("Configurations/Configuration/Key[text() = 'VTC_Light_DayNight']");

		if (value == null || value.equals(""))
		{
			//default
			vtcdaynight[0] = 100;
			vtcdaynight[1] = 100;
		}
		else
		{
			value = value.replace("[", "");
			value = value.replace("]", "");
			String[] values = value.split(";");
			if (values.length != 2)
			{
				//default
				vtcdaynight[0] = 100;
				vtcdaynight[1] = 100;
			}
			else
			{
				try
				{
					vtcdaynight[0] = Integer.parseInt(values[0]);
					vtcdaynight[1] = Integer.parseInt(values[1]);
				}
				catch(Exception ex)
				{
					//default
					vtcdaynight[0] = 100;
					vtcdaynight[1] = 100;
				}
			}

		}
		return vtcdaynight;
	}
}