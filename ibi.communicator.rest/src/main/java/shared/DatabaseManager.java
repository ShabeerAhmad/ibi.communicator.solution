package shared;

import java.sql.*;

import javax.security.auth.login.Configuration;

import shared.ApplicationUtility;
import shared.Logger;

public class DatabaseManager 
{
	   // JDBC driver name and database URL
	   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	   //static final String DB_URL = "jdbc:mysql://localhost/communication";
	   /*
	   static final String DB_URL = AppSettings.getDatabaseName();  //"jdbc:mysql://localhost/communicationstaging";
	   //  Database credentials
	   static final String USER = AppSettings.getDatabaseUser();//"communication";
	   static final String PASS = AppSettings.getDatabaseUserPassword();//"ibi0786";
	   */
	   
	   /* Live
	   static final String DB_URL = "jdbc:mysql://localhost/communication";
	   //  Database credentials
	   static final String USER = "communication";
	   static final String PASS = "ibi0786";
	   */
	   
	   /* staggin
	   static final String DB_URL = "jdbc:mysql://localhost/communicationstaging";
	   //  Database credentials
	   static final String USER = "staging";
	   static final String PASS = "staging123";
	   */
	   
	   
	   static final String DB_URL = "jdbc:mysql://localhost/communication";
	   //  Database credentials
	   static final String USER = "communication";
	   static final String PASS = "ibi0786";
	   
	   
	   private static Connection conn = null;
	   private static CallableStatement stmt = null;
	   private static ResultSet rs = null;
	   
	   private static Connection getDBConnection()
	   {
		   Connection conn = null;
		   try
		   {
		      //STEP 1: Register JDBC driver
		      Class.forName(JDBC_DRIVER);
		      Logger.write("communication_rest", "Step -01 Register JDBC driver and DB_URL: " + DB_URL + " USER: " + USER + " PASS: " + PASS);
		      
		      //STEP 2: Open a connection
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      Logger.write("communication_rest", "Step -02 Open a connection");
	
		      return conn;
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "Exception DB: " + se.getMessage());
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   Logger.write("communication_rest", "Exception DB: " + e.getMessage());
			   Logger.writeError("communication_rest",  e);
		   }
		   return null;
	   }
   
	   public static String authenticate(String userName, String password,String mumbleUsername)
	   {
		   try
		   {
			   try
			   {
			      conn = getDBConnection();
			      Logger.write("communication_rest", "Step -02 Open a connection");
		
			      //STEP 3: Execute a query
			      stmt = conn.prepareCall("{call authenticate(?,?,?)}");
			      stmt.setString(1, userName);
			      stmt.setString(2, password);
			      stmt.setString(3, mumbleUsername);
			      rs = stmt.executeQuery();
			      Logger.write("communication_rest", "Qeury executed");
			      //STEP 4: Extract data from result set
			      while(rs.next())
			      {
			    	String auth = rs.getString("auth");
			    	if (!auth.equalsIgnoreCase("0"))
			    		return auth;
			    		//return ApplicationUtility.issueToken();
			      }
			      return "";
			      //STEP 6: Clean-up environment
			   }
			   catch(SQLException se)
			   {
				   Logger.write("communication_rest", "Exception DB: " + se.getMessage());
				   Logger.writeError("communication_rest",  se);
			   }
			   catch(Exception e)
			   {
				   //return "NOT AUTH";
				   Logger.write("communication_rest", "Exception DB: " + e.getMessage());
				   Logger.writeError("communication_rest",  e);
			   }
			   finally
			   {
				   cleanup();
			   }//end try
			   return "";
		   }
		   catch(Exception se)
		   {
	    	  Logger.writeError("communication_rest",  se);
		   }//end finally try
		   return "{}";
	   }
	   
	   public static String getConfigs()
	   {
		   try
		   {
			   Logger.write("communication_rest", "getConfigs >> Step -01 Trying to Open a connection");
		      conn = getDBConnection();
		      Logger.write("communication_rest", "getConfigs >> Step -02 Open a connection");
	
		      //STEP 3: Execute a query
		      stmt = conn.prepareCall("{call getconfigs()}");
		      rs = stmt.executeQuery();
		      Logger.write("communication_rest", "getConfigs >> Qeury executed");
		      //STEP 4: Extract data from result set
		      String jsonString = "";
		      while(rs.next())
		      {
		  		jsonString  +=	"\""+ rs.getString("key") +"\":\""+ rs.getString("value") + "\"" ;
		  		if (!rs.isLast()) jsonString += ",";
		      }
		      if (!ApplicationUtility.isNullOrEmpty(jsonString))
		    	  jsonString = "{" + jsonString + "}";
		      return jsonString;
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "getConfigs >> Exception DB: " + se.getMessage());
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_rest", "getConfigs >> Exception DB: " + e.getMessage());
			   Logger.writeError("communication_rest",  e);
		   }
		   finally
		   {
			   cleanup();
		   }//end try
		   return "{}";
	   }
	   
	   public static String getBusList(int groupId)
	   {
		   try
		   {
		      conn = getDBConnection();
		      Logger.write("communication_rest", "Step -02 Open a connection");
	
		      //STEP 3: Execute a query
		      stmt = conn.prepareCall("{call vehicleslistbygroupid(?,?)}");
		      stmt.setInt(1, groupId);
		      stmt.setInt(2, ClientConfigurations.getAgeToOffline()); // Need to pass configuration of offlineage
		      rs = stmt.executeQuery();
		      Logger.write("communication_rest", "Qeury executed");
		      //STEP 4: Extract data from result set
		      String buses = "";
		      while(rs.next())
		      {
		  		String jsonString  =	"{"
						  +	  		"\"vehiclenumber\":\""+ rs.getString("vehiclenumber") +"\","	
						  +	  		"\"customerid\":\""+ rs.getString("customerid") +"\","	
						  +			"\"groups\" : \""+ rs.getString("vehiclescount") +"\"," 
						  +			"\"status\":\""+ rs.getString("status") +"\","
						  +			"\"rute\":\""+ rs.getString("rute") +"\""
						  +	  	"}";
		  		buses += jsonString;
		  		if (!rs.isLast()) buses += ",";
		      }
		      if (!ApplicationUtility.isNullOrEmpty(buses))
		    	  buses = "[" + buses + "]";
		      return buses;
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "Exception DB: " + se.getMessage());
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_rest", "Exception DB: " + e.getMessage());
			   Logger.writeError("communication_rest",  e);
		   }
		   finally
		   {
			   cleanup();
		   }//end try
		   return "{}";
	   }
	   
	   
	   public static String vehicleListWithStatus(int groupId)
	   {
		   try
		   {
		      conn = getDBConnection();
		      Logger.write("communication_rest", "Step -02 Open a connection");
	
		      //STEP 3: Execute a query
		      stmt = conn.prepareCall("{call vehicleslistbygroupid(?,?)}");
		      stmt.setInt(1, groupId);
		      stmt.setInt(2, ClientConfigurations.getAgeToOffline()); // Need to pass configuration of offlineage
		      rs = stmt.executeQuery();
		      Logger.write("communication_rest", "Qeury executed");
		      //STEP 4: Extract data from result set
		      String finalJson = "";
		      
		      while(rs.next())
		      {
		  		String jsonString  =	"{"
						  +	  		"\"vehiclenumber\":\""+ rs.getString("vehiclenumber") +"\","	
						  +	  		"\"customerid\":\""+ rs.getString("customerid") +"\","	 
						  +			"\"currentstatus\":\""+ rs.getString("status") +"\""
						  +	  	"}";
		  		finalJson += jsonString;
		  		if (!rs.isLast()) finalJson += ",";
		      }
		      if (!ApplicationUtility.isNullOrEmpty(finalJson))
		    	  finalJson = "{\"command\":\"vehiclelist\", \"vehicles\":"  + "[" + finalJson + "]" + "}";
		      return finalJson;
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "Exception DB: " + se.getMessage());
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_rest", "Exception DB: " + e.getMessage());
			   Logger.writeError("communication_rest",  e);
		   }
		   finally
		   {
			   cleanup();
		   }//end try
		   return "{}";
	   }
	   
	   
	   public static String getOperatorsList()
	   {
		   try
		   {
		      conn = getDBConnection();
		      Logger.write("communication_rest", "Step -02 Open a connection");
	
		      //STEP 3: Execute a query
		      stmt = conn.prepareCall("{call operatorlist()}");
		      rs = stmt.executeQuery();
		      Logger.write("communication_rest", "Qeury executed");
		      //STEP 4: Extract data from result set
		      String operators = "";
		      while(rs.next())
		      {
		  		String jsonString  =	"{"
						  +	  		"\"id\":\""+ rs.getString("operatorid") +"\","	
						  +	  		"\"name\":\""+ rs.getString("operatorname") +"\","	
						  +			"\"mumbleusername\":\""+ rs.getString("mumbleusername") +"\""
						  +	  	"}";
		  		operators += jsonString;
		  		if (!rs.isLast()) operators += ",";
		      }
		      if (!ApplicationUtility.isNullOrEmpty(operators))
		    	  operators = "[" + operators + "]";
		      return operators;
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "Exception DB: " + se.getMessage());
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_rest", "Exception DB: " + e.getMessage());
			   Logger.writeError("communication_rest",  e);
		   }
		   finally
		   {
			   cleanup();
		   }//end try
		   return "";
	   }
	   
	   public static String getOperationsOfficeList()
	   {
		   try
		   {
		      conn = getDBConnection();
		      Logger.write("communication_rest", "Step -02 Open a connection");
	
		      //STEP 3: Execute a query
		      stmt = conn.prepareCall("{call operationsofficelist()}");
		      rs = stmt.executeQuery();
		      Logger.write("communication_rest", "Qeury executed");
		      //STEP 4: Extract data from result set
		      String operations = "";
		      while(rs.next())
		      {
		  		String jsonString  =	"{"
						  +	  		"\"id\":\""+ rs.getString("operatorgroupid") +"\","	
						  +	  		"\"name\":\""+ rs.getString("operatorgroupname") +"\""
						  +	  	"}";
		  		operations += jsonString;
		  		if (!rs.isLast()) operations += ",";
		      }
		      if (!ApplicationUtility.isNullOrEmpty(operations))
		    	  operations = "[" + operations + "]";
		      return operations;
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "Exception DB: " + se.getMessage());
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_rest", "Exception DB: " + e.getMessage());
			   Logger.writeError("communication_rest",  e);
		   }
		   finally
		   {
			   cleanup();
		   }//end try
		   return "";
	   }
	   
	   public static String getGroupData()
	   {
		   try
		   {
		      conn = getDBConnection();
		      Logger.write("communication_rest", "Step -02 Open a connection");
	
		      //STEP 3: Execute a query
		      stmt = conn.prepareCall("{call groupsdata()}");
		      rs = stmt.executeQuery();
		      Logger.write("communication_rest", "Qeury executed");
		      //STEP 4: Extract data from result set
		      String groups = "";
		      while(rs.next())
		      {
		  		String jsonString  =	"{"
						  +	  		"\"groupid\":\""+ rs.getString("groupid") +"\","	
						  +	  		"\"groupname\":\""+ rs.getString("groupname") +"\","	
						  +			"\"vehiclescount\":\""+ rs.getString("vehiclescount") +"\""
						  +	  	"}";
		  		groups += jsonString;
		  		if (!rs.isLast()) groups += ",";
		      }
		      if (!ApplicationUtility.isNullOrEmpty(groups))
		    	  groups = "[" + groups + "]";
		      return groups;
		      //STEP 6: Clean-up environment
		   }
		   catch(SQLException se)
		   {
			   Logger.write("communication_rest", "Exception DB: " + se.getMessage());
			   Logger.writeError("communication_rest",  se);
		   }
		   catch(Exception e)
		   {
			   //return "NOT AUTH";
			   Logger.write("communication_rest", "Exception DB: " + e.getMessage());
			   Logger.writeError("communication_rest",  e);
		   }
		   finally
		   {
			   cleanup();
		   }//end try
		   return "";
	   }
	   private static void cleanup()
	   {
		 //finally block used to close resources
		  try
	      {
	         if(rs!=null)
	        	 rs.close();
	      }
	      catch(SQLException se2)
	      {
	    	  Logger.writeError("communication_rest",  se2);
	      }// nothing we can do
	      try
	      {
	         if(stmt!=null)
	            stmt.close();
	      }
	      catch(SQLException se2)
	      {
	    	  Logger.writeError("communication_rest",  se2);
	      }// nothing we can do
	      try
	      {
	         if(conn!=null)
	            conn.close();
	      }
	      catch(SQLException se)
	      {
	    	  Logger.writeError("communication_rest",  se);
	      }//end finally try
	   }
}  
