package models;

public class Bus 
{
	@Override
	public String toString()
	{
		String jsonString = "["
						  + 	"{"
						  +	  		"\"busnumber\":\"111\","				
						  +			"\"groups\" : \"4\"," 
						  +			"\"status\":\"ONLINE\","
						  +			"\"rute\":\"IN RUTE\""
						  +	  	"}"
						  +	"]";
		return jsonString;
	}
}
