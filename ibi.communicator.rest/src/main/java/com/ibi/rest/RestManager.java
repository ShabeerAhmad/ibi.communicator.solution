package com.ibi.rest;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import models.Bus;
import models.BusGroup;
import models.Credentials;
import shared.ApplicationUtility;
import shared.DatabaseManager;
import shared.Logger;
import shared.PersistenceManager;

public class RestManager 
{
	public static String getConfigs() 
	{
		try
		{
			return DatabaseManager.getConfigs();
		}
		catch (Exception e) 
	    {
	    	Logger.write("communication_rest", "Exception getConfigs failed : " + e.getMessage());
	    	return "{}";
        }  
	}
	
	public static String authenticate(Credentials credentials) {

	    String username = credentials.getUsername();
	    String password = credentials.getPassword();
	    String mumbleUsername = credentials.getMumbleusername(); // need to change as it come from UI
	    Logger.write("communication_rest", "username : " + username + " password : " + password + " mumbleUsername: " + mumbleUsername);
	    try 
	    {
	    	// Issue a token for the user
	    	String auth = DatabaseManager.authenticate(username, password,mumbleUsername);
	    	String token = ApplicationUtility.isNullOrEmpty(auth) ? "" : ApplicationUtility.issueToken();
            token = "{"+
                     "\"token\": \""+ token +"\"," +
                     "\"username\": \""+ auth +"\"" +
            		 "}";
            
            Logger.write("communication_rest", "token : " + token);
            return token;
        } 
	    catch (Exception e) 
	    {
	    	Logger.write("communication_rest", "UNAUTHORIZED");
	    	return "{\"token\":\"\",\"username\":\"\"}";
        }  
	}
	public static String authenticateUser(Credentials credentials) {

	    String username = credentials.getUsername();
	    String password = credentials.getPassword();
	    String token = "{\"token\":\"\"}";
	    Logger.write("communication_rest", "username : " + username + " password : " + password);
	    try 
	    {
            // Authenticate the user using the credentials provided
	    	if (!("msa".equals(username)) || !("msa123".equals(password)))
	    	{
	    		Logger.write("communication_rest", "UNAUTHORIZED");
	    		return token;
	    	}
	    	
	    	// Issue a token for the user
            token = issueToken();
            token = "{\"token\": \""+ token +"\"}";
            Logger.write("communication_rest", "token : " + token);
            return token;
            
            // Return the token on the response
            //return Response.ok(token).build();
        } 
	    catch (Exception e) 
	    {
	    	Logger.write("communication_rest", "UNAUTHORIZED");
	    	return token;
            //return Response.status(Response.Status.UNAUTHORIZED).build();
        }  
	    // Authenticate the user, issue a token and return a response
	}
	
	
	public static String getBusGroups()
    {
	   try
		{
		   return DatabaseManager.getGroupData();
		}
		catch(Exception se)
		{
			Logger.writeError("communication_rest",  se);
		}//end finally try
	   return "{}";
    }
	
	public static String getOperatorsList()
    {
	   try
		{
		   return DatabaseManager.getOperatorsList();
		}
		catch(Exception se)
		{
			Logger.writeError("communication_rest",  se);
		}//end finally try
	   return "{}";
    }
	
	public static String getOperationsOfficeList()
    {
	   try
		{
		   return DatabaseManager.getOperationsOfficeList();
		}
		catch(Exception se)
		{
			Logger.writeError("communication_rest",  se);
		}//end finally try
	   return "{}";
    }
	
	
	
	public static String getBusList(int groupId)
	{
		try
		{
			return DatabaseManager.getBusList(groupId);
		}
		catch(Exception se)
		{
			Logger.writeError("communication_rest",  se);
		}//end finally try
	   return "{}";
	}
	
	/* private supporting document*/
	private static String issueToken() 
    {
    	Random random = new SecureRandom();
    	String token = new BigInteger(130, random).toString(32);
    	return token;
    }
	
	/*
	@POST
	@Path("/authenticate")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces({MediaType.TEXT_PLAIN})
	public String authenticateUser(Credentials credentials) {

	    String username = credentials.getUsername();
	    String password = credentials.getPassword();
	    Logger.write("communication_rest", "username : " + username + " password : " + password);
	    try 
	    {
            // Authenticate the user using the credentials provided
            //authenticate(username, password);
	    	if (!("msa".equals(username)) || !("msa123".equals(password)))
	    	{
	    		Logger.write("communication_rest", "UNAUTHORIZED");
	    		return "UNAUTHORIZED";
	    	}
	    	// Issue a token for the user
            String token = issueToken();
            Logger.write("communication_rest", "token : " + token);
            return token;
            // Return the token on the response
            //return Response.ok(token).build();
        } 
	    catch (Exception e) 
	    {
	    	Logger.write("communication_rest", "UNAUTHORIZED");
	    	return "UNAUTHORIZED";
            //return Response.status(Response.Status.UNAUTHORIZED).build();
        }  
	    // Authenticate the user, issue a token and return a response
	}
	 */
}
