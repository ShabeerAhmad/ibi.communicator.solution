package com.ibi.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import models.Credentials;
import shared.DatabaseManager;
import shared.Logger;

@Path("/")
public class JSONService 
{
	
	@GET
	@Path("/test")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String getTesting()
	{
		Logger.write("communication_rest", "@GET: test API called");
		return "OK";
	}
	
	@POST
	@Path("/auth")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces({MediaType.TEXT_PLAIN})
	public String authenticateUser(Credentials credentials) 
	{
	    return RestManager.authenticateUser(credentials);
	}
	
	@POST
	@Path("/authenticate")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces({MediaType.TEXT_PLAIN})
	public String authenticate(Credentials credentials) 
	{
		Logger.write("communication_rest", "@POST: authenticate API called");
	    return RestManager.authenticate(credentials);
	}
	
	
	@GET
	@Path("/busgroups")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String getBusGroups()
	{
		Logger.write("communication_rest", "@GET: busgroups API called");
		return RestManager.getBusGroups();
	}
	
	@GET
	@Path("/buses/{groupId}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String getBusList(@PathParam("groupId") int groupId)
	{
		Logger.write("communication_rest", "@GET: getBuses list API called");
		return RestManager.getBusList(groupId);
	}
	
	
	@GET
	@Path("/operators")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String getOperatorsList()
	{
		Logger.write("communication_rest", "@GET: getOperatorsList API called");
		return RestManager.getOperatorsList();
	}
	
	@GET
	@Path("/operationoffices")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String getOperationsOfficeList()
	{
		Logger.write("communication_rest", "@GET: getOperationsOfficeList API called");
		return RestManager.getOperationsOfficeList();
	}
	
	
	@GET
	@Path("/configs")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String getConfigs()
	{
		Logger.write("communication_rest", "@GET: getConfigs API called");
		return RestManager.getConfigs();
	}
	
	/*
	@GET
	@Path("/buses")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String moviajourneys(@QueryParam("line") String line)
	{
		Logger.write("dcu2_rest", "@GET: Movia journeys by line and geo position API called");
		return MoviaJourneyService.getJourneysByLineAndGeoPosition(line);
	}
	*/
}
